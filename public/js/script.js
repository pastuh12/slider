"use strict";
window.addEventListener('DOMContentLoaded', function (event) {
    let position = 0;
    let slidesToShow = 4;
    const slidesToScroll = 1;
    if (document.body.offsetWidth < 900){
        slidesToShow = 2;
    }
    const container = document.getElementById("container");
    const field = document.querySelector(".field");
    const item = document.querySelectorAll(".Slider");
    const img = document.getElementsByTagName("img");
    const itemsCount = item.length;
    const btnPrev = document.querySelector(".Prev");
    const btnNext = document.querySelector(".Next");
    const containerWidth = container.offsetWidth;
    const itemWidth = containerWidth / slidesToShow;
    const movePosition = slidesToScroll * itemWidth + 2;
    const dots = document.querySelectorAll(".dot")
    console.log(itemWidth);
    item.forEach((item) => {
        item.style.maxWidth = `${itemWidth}px`;
        });
        for(var i=0; i<img.length; i++){
            img[i].style.width=`${itemWidth}px`;
        }

    btnPrev.addEventListener('click', () => {
        const itemsLeft = Math.abs(position) / itemWidth;
        position += itemsLeft >= slidesToScroll ? movePosition : itemsLeft * itemWidth;
        setPosition();
        checkbtn();
    });

    btnNext.addEventListener('click', () => {
        const itemsLeft = (Math.abs(position) + slidesToShow * itemWidth) / itemWidth;
        position -= itemsLeft >= slidesToScroll ? movePosition: itemsLeft * itemWidth;
        setPosition();
        checkbtn();
        console.log(container.style.backgroundColor);
    });

    const setPosition = () =>{
        field.style.transform = `translateX(${position}px)`;
    };
    const checkbtn = () => {
        btnPrev.disabled = position === 0;
        btnNext.disabled = position <= -(itemsCount - slidesToShow) * itemWidth;
    };
    checkbtn();
});